-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.43 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица invoice.invoices
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date NOT NULL,
  `discount` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `num` (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы invoice.invoices: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
REPLACE INTO `invoices` (`id`, `num`, `status`, `date`, `discount`) VALUES
	(3, 1, 1, '2020-12-20', 10),
	(4, 222, 1, '2021-02-04', 5),
	(11, 333, 1, '2020-12-20', 10),
	(12, 123, 2, '2019-12-20', 10),
	(13, 1342, 1, '2020-12-20', 10),
	(14, 155, 2, '2020-10-20', 18),
	(47, 5, 2, '2021-01-20', 5);
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;

-- Дамп структуры для таблица invoice.items
DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы invoice.items: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
REPLACE INTO `items` (`id`, `name`, `price`) VALUES
	(1, 'салат Цезарь', 300),
	(2, 'Яблочный сок', 150),
	(3, 'Кофе гляссе', 150),
	(4, 'Эклер', 150),
	(5, 'пирожное Павлова', 300);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Дамп структуры для таблица invoice.item_in_invoice
DROP TABLE IF EXISTS `item_in_invoice`;
CREATE TABLE IF NOT EXISTS `item_in_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы invoice.item_in_invoice: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `item_in_invoice` DISABLE KEYS */;
REPLACE INTO `item_in_invoice` (`id`, `invoice_id`, `item_id`, `quantity`) VALUES
	(29, 14, 1, 2),
	(31, 14, 5, 10),
	(32, 14, 3, 15),
	(33, 47, 4, 5),
	(34, 47, 2, 3),
	(35, 47, 5, 1);
/*!40000 ALTER TABLE `item_in_invoice` ENABLE KEYS */;

-- Дамп структуры для таблица invoice.status
DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы invoice.status: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
REPLACE INTO `status` (`id`, `name`) VALUES
	(1, 'Sent'),
	(2, 'Paid'),
	(3, 'Cancelled');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
