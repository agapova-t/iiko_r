<?php
namespace App\Controller;

use Base\AbstractController;
use App\Model\Invoice as Inv;
use App\Model\Item;

class Invoice extends AbstractController
{
    function indexAction()
    {

       $invoces = Inv::GetAllInvoices();
       $invoces = Inv::GetAllInvoices(2, '2020-01-01');
        $invoicelines = '';
        foreach ($invoces as $invoice){
            $itemtable='';
            $arrInvoces = $invoice->getComposition();
            if($arrInvoces){
                foreach ($arrInvoces as $wholeItem){
                    $this->view->assign('name', $wholeItem['item']->getName());
                    $this->view->assign('price', $wholeItem['item']->getPrice());
                    $this->view->assign('quantity', $wholeItem['quantity']);

                    $itemtable .= $this->view->render('/Invoice/itemtable.phtml', []);
                }
            }




            $this->view->assign('itemtable', $itemtable);
            $this->view->assign('num', $invoice->getNum());
            $this->view->assign('status', $invoice->getStatus());
            $this->view->assign('date', date('d.m.Y', strtotime($invoice->getDate())));
            $this->view->assign('discount', $invoice->getDiscount());
            $this->view->assign('sum', $invoice->sum());


            $invoicelines .= $this->view->render('/Invoice/invoiceline.phtml', []);
        }

        $this->view->assign('invoicelines', $invoicelines);

        return $this->view->render('/Invoice/index.phtml', []);

    }


}