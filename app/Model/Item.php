<?php
namespace App\Model;

use Base\AbstractModel;
use Base\Db;

class Item extends AbstractModel
{
    private $id;
    private $name;
    private $price;

    public function __construct($data = [])
    {
        if ($data) {
            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->price = $data['price'];
        }
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): self
    {
        $this->price = $price;
        return $this;
    }



    public static function GetById(int $id): self
    {
        $db = Db::getInstance();
        $select = "SELECT * FROM items WHERE id = $id";
        $data = $db->fetchOne($select, __METHOD__);

        if (!$data) {
            return null;
        }

        return new self($data);
    }

    public static function GetByInvoiceId(int $id): ?array
    {
        $db = Db::getInstance();
        $select = "SELECT i.id, i.`name`, i.price, ii.quantity FROM items i INNER JOIN item_in_invoice ii ON i.id=ii.item_id
WHERE ii.invoice_id = $id";
        $data = $db->fetchAll($select, __METHOD__);

        if (!$data) {
            return null;
        }else{
            $count = count($data);
            if($count){
                $arrItems = [];
                for ($i = 0; $i<$count; $i++){
                    $arrItems[]= ['item' => new self($data[$i]), 'quantity' => $data[$i]['quantity']];
                }
            }
        }

        return $arrItems;
    }


    public function save(){
        $db = Db::getInstance();
        $insert = "INSERT INTO items (`name`, `price`) VALUES (
            :name, :price
        )";
        $date =  date('Y-m-d', strtotime($this->date));
        $db->exec($insert, __METHOD__, [
            ':name' => $this->name,
            ':price' => $this->price
        ]);

        $id = $db->lastInsertId();
        $this->id = $id;

        return $id;

    }



}