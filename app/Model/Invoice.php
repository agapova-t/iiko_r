<?php
namespace App\Model;

use Base\AbstractModel;
use Base\Db;
use App\Model\Item;

class Invoice extends AbstractModel
{
    private $id;
    private $num;
    private $status;
    private $date;
    private $discount;
    private $composition = [];

    public function __construct($data = [], $composition = [])
    {
        if ($data) {
            $this->id = $data['id'];
            $this->num = $data['num'];
            $this->status = $data['status'];
            $this->date = $data['date'];
            $this->discount = $data['discount'] ?? 0;
            if($composition)$this->composition = $composition;
        }
    }

    /**
     * @return array
     */
    public function getComposition(): array
    {
        return $this->composition;
    }

    /**
     * @param array $composition
     */
    public function setComposition(array $composition): self
    {
        $this->composition = $composition;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param mixed $num
     */
    public function setNum($num): self
    {
        $this->num = $num;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function save(){
        $db = Db::getInstance();
        $insert = "INSERT INTO invoices (`num`, `status`, `date`, `discount`) VALUES (
            :num, :status, :dat, :discount
        )";
        $date =  date('Y-m-d', strtotime($this->date));
        $db->exec($insert, __METHOD__, [
            ':num' => $this->num,
            ':status' => $this->status,
            ':dat' => $date,
            ':discount' => $this->discount
        ]);

        $id = $db->lastInsertId();
        $this->id = $id;

        return $id;

    }

    public function AddItem(int $itemId, $quantity): self
    {
        $item = Item::GetById($itemId);
        $count = count($this->composition);
        if($item){
            if($count){
                for($i=0; $i<$count; $i++){
                    if($this->composition[$i]['item']->getId() == $itemId){
                        $this->composition[$i]['quantity'] += $quantity;
                        return $this;
                    }
                }

            }
            $this->composition[] = [
                'item' => $item,
                'quantity' => $quantity
            ];
        }

        return $this;
    }

    public function saveItems()
    {
       $id = $this->id;
       $arr = Item::GetByInvoiceId($id);
       if($arr){//проверка и добовление
           $arrId=[];
           foreach ($arr as $wholeItem){
               $arrId[]=$wholeItem['item']->getId();
           }
           foreach ($this->composition as $wholeItem){
               if(in_array($wholeItem['item']->getId(), $arrId)){
                   $invoiceItemId = $this->getInvoiceItemId($wholeItem['item']->getId());
                   $this->updateOneItem($invoiceItemId, $wholeItem['quantity']);
               }else
               $this->addOneItem($wholeItem['item']->getId(), $wholeItem['quantity']);
           }

       }else{
           foreach ($this->composition as $wholeItem){
               $this->addOneItem($wholeItem['item']->getId(), $wholeItem['quantity']);
           }
       };

    }

    private function addOneItem(int $itemId, int $quantity)
    {
        $db = Db::getInstance();
        $insert = "INSERT INTO item_in_invoice (`invoice_id`, `item_id`, `quantity`) VALUES (
            :invoice_id, :item_id, :quantity
        )";
        $db->exec($insert, __METHOD__, [
            ':invoice_id' => $this->id,
            ':item_id' => $itemId,
            ':quantity' => $quantity
        ]);

    }

    private function updateOneItem(int $invoiceItemId, int $quantity){
        $db = Db::getInstance();
        $insert = "UPDATE item_in_invoice SET `quantity` = :quantity WHERE id = :invoiceItemId";

        $db->exec($insert, __METHOD__, [
            ':quantity' => $quantity,
            ':invoiceItemId' => $invoiceItemId
        ]);

    }

    private function getInvoiceItemId(int $ItemId):int
    {
        $db = Db::getInstance();
        $select = "SELECT id FROM item_in_invoice WHERE invoice_id = $this->id AND  item_id = $ItemId";
        $data = $db->fetchOne($select, __METHOD__);

        if (!$data) {
            return null;
        }else{
            return $data['id'];
        }
    }

    public function DeleteItem(int $itemId){
        foreach ($this->composition as $key=>$value){
            if($value['item']->getId() == $itemId){
                unset($this->composition[$key]);
            }
        }

        if($this->getInvoiceItemId($itemId)){
            $db = Db::getInstance();
            $delete = "DELETE FROM item_in_invoice WHERE id = ". $this->getInvoiceItemId($itemId);

            $db->exec($delete, __METHOD__, []);
        }
    }

    public function sum():int
    {
        $sum = 0;
        foreach ($this->composition as $wholeItem){
            $sum += $wholeItem['item']->getPrice() * $wholeItem['quantity'];
        }
        $sum = $sum*(100-$this->getDiscount())/100;
        return $sum;
    }

//Получение нескольких записей
    public static function GetAllInvoices(int $status=0, $startDate=''){
        if($status <1 || $status>3)$status = 0;
        if($status){
            $statusTail = "status = $status";
        }else{
            $statusTail = "TRUE";
        }
        if($startDate){
            $startDate =  date('Y-m-d', strtotime($startDate));
            $startTail = "date > '$startDate'";
        }else{
            $startTail = 'TRUE';
        }
        $Tail = "WHERE $statusTail AND $startTail";

        $db = Db::getInstance();
        $select = "SELECT * FROM invoices $Tail ";
        $data = $db->fetchAll($select, __METHOD__);

        if (!$data) {
            return null;
        }else{
            $arrInvoces=[];
            foreach ($data as $item){
                $arrItems = Item::GetByInvoiceId($item['id']);
                $arrInvoces[] = new self($item, $arrItems);
            }
        }

        return $arrInvoces;
    }
}