<?php
namespace Base;

class Application
{
    private $route;
    /** @var AbstractController */
    private $controller;
    private $actionName;

    public function __construct()
    {
        $this->route = new Route();
    }

    public function run()
    {

        try{
            session_start();
            $this->addRoutes();
            $this->initController();
            $this->initAction();
            $view = new View();
            $this->controller->setView($view);



            $content = $this->controller->{$this->actionName}();
            return $content;

        }catch (RedirectException $e){
            header("Location: ". $e->getUrl());
            die;
        }catch (RouteException $e) {
            header("HTTP/1.0 404 Not Found");
            echo $e->getMessage();
        }
    }

    private function addRoutes()
    {
         $this->route =new Route();
        $this->route->addRoute('/html', \App\Controller\Invoice::class, 'index');
        $this->route->addRoute('/html/invoice', \App\Controller\Invoice::class, 'index');
        $this->route->addRoute('/html/invoice/index', \App\Controller\Invoice::class, 'index');
    }

    private function initController()
    {
        $controllerName = $this->route->getControllerName();
        if (!class_exists($controllerName)) {
            throw new RouteException('Cant find controller ' . $controllerName);
        }

        $this->controller = new $controllerName();
    }

    private function initAction()
    {
        $actionName = $this->route->getActionName();
        if (!method_exists($this->controller, $actionName)) {
            throw new RouteException('Action ' . $actionName . ' not found in ' . get_class($this->controller));
        }

        $this->actionName = $actionName;
    }





/*

*/
}